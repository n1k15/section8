FROM nginx:latest

ADD ./src/*.* /usr/share/nginx/html/

WORKDIR /usr/share/nginx/html
